const path = require('path');
const express = require('express');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const app = express();

const conn = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'komune_chat'
});



//set views file
app.set('views', path.join(__dirname, 'views'));

//set view engine
app.set('view engine', 'hbs');

hbs.registerHelper('ifEquals', function (a, b, options) {
    console.log(b);
    if (a !== b) {
        return options.fn(this);
    }

    return options.inverse(this);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/assets', express.static(__dirname + '/assets'));


app.get('/', (req, res) => {
    let sql = "SELECT * FROM t_user";
    conn.getConnection(function (err, connection) {
        conn.query(sql, function (error, results) {
            res.render('index', {
                userResult: results
            });
        });
    })

});

app.get('/register', function (req, res) {
    res.render('register');
});


function userList(user_id) {
    return new Promise(resolve => {
        let sql = "SELECT (select " + user_id + ") as curr_id, id,fullname,username FROM t_user where id <> " + user_id;
        let result = [];
        conn.query(sql, (err, results) => {
            resolve(results);
        });
    });
}
function userListMessage(user_id, message_to) {
    return new Promise(resolve => {
        let sql = "SELECT (select " + user_id + ") as curr_id,msg.user_id,msg.id,msg.chat_text,msg.message_to,usr.fullname,msg.chat_at FROM t_message as msg left join t_user as usr on msg.user_id = usr.id where (msg.user_id = " + user_id + " and msg.message_to=" + message_to + ") or  (msg.user_id = " + message_to + " and msg.message_to=" + user_id + ")";
        let result = [];
        conn.query(sql, (err, results) => {
            console.log(err);
            resolve(results);
        });
    });
}

app.get('/chat', async function (req, res) {
    //console.log(req.query.user_id);
    let user_id = req.query.user_id;
    let message_to = req.query.message_to;
    let user = await userList(user_id);
    let message = await userListMessage(user_id, message_to);
    res.render('chat', {
        listUser: user, list_massage: message, state_user: user_id, state_message_to: message_to
    });
    // "and message_to=" + message_to

});



app.post('/insertUser', (req, res) => {
    // console.log("isinya" + req.body.username);
    let user_name = JSON.stringify(req.body.username);
    let fullname = JSON.stringify(req.body.fullname);
    let sql = "INSERT INTO t_user (username,fullname) VALUES (" + user_name + "," + fullname + ")";
    conn.getConnection(function (err, connection) {
        conn.query(sql, function (error, results) {
            res.redirect('/');
            console.log('success diinserts');
        });
    })
});


app.post('/insertMessage', (req, res) => {
    let user_id = JSON.stringify(req.body.user_id);
    let message_to = JSON.stringify(req.body.message_to);
    let message_text = JSON.stringify(req.body.message_text);
    let sql = "INSERT INTO t_message (user_id,message_to,chat_text) VALUES (" + user_id + "," + message_to + "," + message_text + ")";
    conn.getConnection(function (err, connection) {
        conn.query(sql, function (error, results) {
            res.redirect('/chat?user_id=' + req.body.user_id + "&&message_to=" + req.body.message_to);
            console.log(error);
        });
    })
});

app.listen(3000, () => {
    console.log('Server is running at port 8000');
});