-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2021 at 11:30 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `komune_chat`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_message`
--

CREATE TABLE `t_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `chat_text` text NOT NULL,
  `message_to` int(11) NOT NULL,
  `chat_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_message`
--

INSERT INTO `t_message` (`id`, `user_id`, `chat_text`, `message_to`, `chat_at`) VALUES
(18, 5, 'yes', 1, '2021-04-03 16:09:52'),
(19, 5, 'test', 2, '2021-04-03 16:09:59'),
(20, 1, 'kuy yuks  ntr malem ', 5, '2021-04-03 16:10:30'),
(21, 1, 'ayy', 2, '2021-04-03 16:10:39'),
(22, 1, 'lyla...', 3, '2021-04-03 16:10:46'),
(23, 3, 'oit', 5, '2021-04-03 16:12:29'),
(24, 3, 'yuks', 5, '2021-04-03 16:12:39'),
(25, 3, 'apaa?', 1, '2021-04-03 16:12:49'),
(26, 2, 'wasdd', 1, '2021-04-03 16:27:58'),
(27, 2, 'wasdddd', 1, '2021-04-03 16:28:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `fullname` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `username`, `fullname`, `created_at`) VALUES
(1, 'robby', 'Robby Haryanto', '2021-04-03 15:50:52'),
(2, 'Aya', 'Aya Liana', '2021-04-03 16:04:38'),
(3, 'Lyla', 'Lyla Dewi Lestari', '2021-04-03 16:04:41'),
(5, 'Oxy', 'Oxy Putri Chayanti', '2021-04-03 16:07:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_message`
--
ALTER TABLE `t_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_message`
--
ALTER TABLE `t_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
